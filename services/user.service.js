const { User } = require("../models");

async function createUser(payload) {
  const user = new User({
    full_name: payload.full_name,
    username: payload.username,
  });
  return user.save();
}

async function getUserByUsername(username) {
  const user = await User.findOne({ username });
  if (!user) {
    return null;
  }
  return user;
}

async function updateRoomsList(username, rooms) {
  const user = await User.findOneAndUpdate({ username }, { rooms_ids: rooms });
  return user;
}

module.exports = {
  createUser,
  getUserByUsername,
  updateRoomsList,
};
