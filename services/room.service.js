const { Room } = require("../models");
const userService = require("./user.service");
const ObjectId = require("../helpers/object-id");

async function getRooms(username) {
  const user = await userService.getUserByUsername(username);
  const rooms_bids = user?.rooms_ids.map((item) => ObjectId(item));
  const rooms = await Room.find({ _id: { $in: rooms_bids } });
  return rooms;
}

async function createRoom(payload) {
  const user = await userService.getUserByUsername(payload.username);
  if (!user) {
    throw new Error("User not found");
  }
  const room = new Room({
    title: payload.title,
    isDirect: payload.isDirect,
  });
  room.users = [user.username];
  const savedRoom = await room.save();
  if (!savedRoom) {
    throw new Error("Error while saving room");
  }
  const roomsIds = [...user.rooms_ids, savedRoom._id.toString()];
  console.log(roomsIds);
  const updatedUser = await userService.updateRoomsList(
    user.username,
    roomsIds
  );
  if (!updatedUser) {
    throw new Error("Error while saving user");
  }
  return savedRoom;
}

async function updateUserList(roomId, users) {
  const room = await Room.findOneAndUpdate(
    { _id: ObjectId(roomId) },
    { users }
  );
  return room;
}

async function inviteUser(payload) {
  const user = await userService.getUserByUsername(payload.username);
  if (!user) {
    throw new Error("User not found");
  }

  const room = await Room.findOne({ _id: ObjectId(payload.room_id) });
  if (!room) {
    throw new Error("Room not found");
  }
  const updatedRoom = await updateUserList(room._id.toString(), [
    ...room.users,
    user.username,
  ]);
  if (!updatedRoom) {
    throw new Error("Error while updating room");
  }
  const roomsIds = [...user.rooms_ids, updatedRoom._id.toString()];
  const updatedUser = await userService.updateRoomsList(
    user.username,
    roomsIds
  );
  if (!updatedUser) {
    throw new Error("Error while updating user");
  }
  return updatedRoom;
}

module.exports = {
  getRooms,
  createRoom,
  inviteUser,
};
