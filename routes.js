const router = require("express").Router();
const { userController, roomController } = require("./controllers");
const { createUserValidation } = require("./middlewares/user.middleware");
const {
  createRoomValidation,
  updateRoomValidation,
} = require("./middlewares/rooms.middleware");

router.get("/", (_req, res) => {
  res.status(200).json({ message: "Api Works" });
});

// User
router.post("/auth/sign-up", createUserValidation, userController.signUp);
router.get("/auth/:username", userController.findByUsername);

// Room
router.get("/rooms/:username", roomController.getRooms);
router.post("/rooms", createRoomValidation, roomController.createRoom);
router.put("/rooms", updateRoomValidation, roomController.inviteUser);

module.exports = router;
