module.exports = class Config {
    static mongoDbUri = process.env.DB_URL;
    static PORT = process.env.PORT | 3000;
};