const validator = require("../helpers/validator");
const createUserValidation = async (req, res, next) => {
  const validationRule = {
    full_name: "required|string",
    username: "required|string",
  };

  await validator(req.body, validationRule, {}, (err, status) => {
    if (!status) {
      res.status(400).send({
        success: false,
        status: 400,
        message: "Bad request",
        code: "ERR_BAD_REQUEST",
        data: err,
      });
    } else {
      next();
    }
  }).catch((err) => console.log(err));
};
module.exports = {
  createUserValidation,
};
