const { roomService } = require("../services");

async function getRooms(req, res) {
  const username = req.params.username;
  try {
    const rooms = await roomService.getRooms(username);
    res.status(200).json(rooms);
  } catch (e) {
    res.status(500).json({
      status: 500,
      message: e?.message,
      code: "ERR_INERNAL_SERVER_ERROR",
    });
  }
}

async function createRoom(req, res) {
  const payload = req.body;
  try {
    const room = await roomService.createRoom({
      username: payload.username,
      title: payload.title,
      isDirect: payload.isDirect,
    });
    res.status(201).json(room);
  } catch (e) {
    res.status(500).json({
      status: 500,
      message: e?.message,
      code: "ERR_INERNAL_SERVER_ERROR",
    });
  }
}

async function inviteUser(req, res) {
  const payload = req.body;
  try {
    const room = await roomService.inviteUser({
      username: payload.username,
      room_id: payload.room_id,
    });
    res.status(200).json(room);
  } catch (e) {
    res.status(500).json({
      status: 500,
      message: e?.message,
      code: "ERR_INERNAL_SERVER_ERROR",
    });
  }
}

module.exports = {
  getRooms,
  createRoom,
  inviteUser,
};

