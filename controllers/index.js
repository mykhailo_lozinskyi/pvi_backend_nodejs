const roomController = require("./room.controller");
const userController = require("./user.controller");

module.exports = {
  userController,
  roomController,
};
