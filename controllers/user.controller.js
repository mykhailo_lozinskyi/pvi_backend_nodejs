const { userService } = require("../services");

async function signUp(req, res) {
  const user = req.body;
  try {
    const userFromDb = await userService.getUserByUsername(user.username);
    if (userFromDb) {
      res.status(400).json({
        status: 400,
        message: "User already exist",
        code: "ERR_BAD_REQUEST",
      });
    } else {
      const createdUser = await userService.createUser(user);
      res.status(201).json(createdUser);
    }
  } catch (e) {
    res.status(500).json({
      status: 500,
      message: e?.message,
      code: "ERR_INERNAL_SERVER_ERROR",
    });
  }
}

async function findByUsername(req, res) {
  const username = req.params.username;
  try {
    const user = await userService.getUserByUsername(username);
    if (!user) {
      res.status(404).json({
        status: 404,
        message: "User not found",
        code: "ERR_NOT_FOUND",
      });
    } else {
      res.status(200).json(user);
    }
  } catch (e) {
    res.status(500).json({
      status: 500,
      message: e?.message,
      code: "ERR_INERNAL_SERVER_ERROR",
    });
  }
}

module.exports = {
  signUp,
  findByUsername,
};