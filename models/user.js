const mongoose = require("mongoose");

const schema = mongoose.Schema({
  full_name: String,
  username: String,
  rooms_ids: Array,
});

module.exports = mongoose.model("User", schema);