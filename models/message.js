const mongoose = require("mongoose");

const schema = mongoose.Schema({
  content: String,
  sender: String,
  roomId: String,
  createdAt: Date,
});

module.exports = mongoose.model("Message", schema);