// set timezone
process.env.TZ = "Europe/Kyiv";

// load configs
require("dotenv").config();

const express = require("express");
const app = express();
const appServer = require("http").createServer(app);
const Router = require("./routes");
const cors = require("cors");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const Config = require("./Config");

const { messagesGateway } = require("./gateways");

app.use(cors());
app.use(bodyParser.json());

mongoose.connect(Config.mongoDbUri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;

db.once("open", () => {
  console.log("Successfully connected to db");
});
messagesGateway.connect(appServer);

app.use(Router);

const server = appServer.listen(Config.PORT, () => {
  console.log(`Server is running on port: ${Config.PORT}`);
});

function shutDown() {
  mongoose.disconnect().then(() => {
    console.log("Disconnected from db");
  });
  messagesGateway.disconnect(() => {
    console.log("Messages gateway is disconnected");
  });
  server.close(() => {
    console.log("Server is shutted down");
  });
}

process.on("exit", () => {
  shutDown();
});

process.on("SIGINT", () => {
  shutDown();
});

process.on("SIGTERM", () => {
  shutDown();
});